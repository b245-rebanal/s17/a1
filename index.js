// console.log("Hello World")
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	// first function here:
	function printUserInfo(){
		let fullName = prompt("What is your name?");
		let age = prompt("How old are you?");
		let location = prompt("Where do you live");

		console.log("Hello, "+ fullName);
		console.log("You are "+ age +" years old.");
		console.log("You live in "+ location);
		alert("Thank you for your input!");
	}

	printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/

	//second function here:

	function printFaveBands(){
		let band1 = "Silent Sanctuary";
		let band2 = "All Time Low";
		let band3 = "Franco";
		let band4 = "Mayonaise";
		let band5 = "December Avenue";

		console.log("1. " + band1)
		console.log("2. " + band2)
		console.log("3. " + band3)
		console.log("4. " + band4)
		console.log("4. " + band5)
	}

	printFaveBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovieRatings(){
		let faveMovie1 = "Interstellar";
		let movie1Rating = 73;
		let faveMovie2 = "About Time";
		let movie2Rating = 70;
		let faveMovie3 = "Avengers: Endgame";
		let movie3Rating = 94;
		let faveMovie4 = "Begin Again";
		let movie4Rating = 83;
		let faveMovie5 = "The Wedding Singer";
		let movie5Rating = 70;

		console.log("1. " + faveMovie1);
		console.log("Rotten Tomato Rating: " + movie1Rating + "%");
		console.log("2. " + faveMovie2);
		console.log("Rotten Tomato Rating: " + movie2Rating + "%");
		console.log("3. " + faveMovie3);
		console.log("Rotten Tomato Rating: " + movie3Rating + "%");
		console.log("4. " + faveMovie4);
		console.log("Rotten Tomato Rating: " + movie4Rating + "%");
		console.log("5. " + faveMovie5);
		console.log("Rotten Tomato Rating: " + movie5Rating + "%");
	}

	faveMovieRatings()
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

